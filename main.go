package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"forwardsms/linemessages"
	"forwardsms/mobilehotspots"

	"github.com/comail/colog"
	"github.com/joho/godotenv"
)

// バージョン埋め込む
// INFO: https://qiita.com/irotoris/items/4aae9ad483bf08915688
var version string
var revision string

func main() {
	// コマンドライン引数を取得
	isVersion := flag.Bool("version", false, "バージョンを表示する")
	flag.Parse()

	// バージョン表示
	if *isVersion {
		fmt.Printf("version: %s-%s\n", version, revision)
		os.Exit(0)
	}

	// 実行ディレクトリを取得する
	exeFile, err := os.Executable()
	if err != nil {
		panic(err)
	}
	// カレントディレクトリを変更する (cron対策)
	exePath := filepath.Dir(exeFile)
	if err := os.Chdir(exePath); err != nil {
		panic(err)
	}

	// ログの初期化
	file := initializeLogging()
	defer file.Close()

	// .envファイルから環境変数を読み込む
	log.Println(".envファイルから環境変数を読み込む")
	loadEnv()
	log.Println(".envファイルから環境変数を読み込み完了")

	// Pocket Wifiの機種を取得する
	deviceType := os.Getenv("DEVICE_TYPE")

	// Wifi機種に合わせてインスタンスを呼び出す
	var mobileHotspot mobilehotspots.ReceiveDeletor
	switch deviceType {
	case "ZTE":
		mobileHotspot = mobilehotspots.NewZte()
	case "HUAWEI":
		mobileHotspot = mobilehotspots.NewHuawei()
	default:
		log.Fatalln("Pocket Wifiの機種が未定義です")
	}

	var lineTry int
	for {
		// SMSを受信する
		log.Println("SMSを受信する")
		nvCnt, sender, phone, content, received := mobileHotspot.ReceiveSMS()

		log.Printf("SMS受信箱%d件 送信者:%s, 電話番号:%s, メッセージ:%s, 受信日時:%s\n", nvCnt, sender, phone, content, received)

		if nvCnt == 0 {
			break
		}

		// LINEへ送信する
		log.Println("LINEへ送信する")
		text := makeNotifiableReceivedMessage(sender, phone, content, received)
		if err := linemessages.SendLineMessage(text); err != nil {
			lineTry++
			if lineTry < 3 {
				continue
			} else {
				break
			}
		}
		lineTry = 0

		// SMSを削除する
		log.Println("SMSを削除する")
		mobileHotspot.DeleteSMS(sender, phone, content, received)

		time.Sleep(time.Millisecond * 500)
	}
}

func initializeLogging() *os.File {
	// logの設定 https://qiita.com/kmtr/items/406073651d7a12aab9c6
	file, err := os.OpenFile("operation_json.log", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		panic(err)
	}

	colog.SetOutput(file)
	colog.ParseFields(true)
	colog.SetFormatter(&colog.JSONFormatter{
		TimeFormat: time.RFC3339,
		Flag:       log.Lshortfile,
	})
	colog.SetDefaultLevel(colog.LDebug)
	colog.SetMinLevel(colog.LTrace)
	// colog.SetFormatter(&colog.StdFormatter{
	// 	Colors: true,
	// 	Flag:   log.Ldate | log.Ltime | log.Lshortfile,
	// })
	colog.Register()

	return file
}

// .envファイルから環境変数を読み込む
func loadEnv() {
	err_read := godotenv.Load()
	if err_read != nil {
		log.Fatalln(".envファイルから環境変数を読み込む:", err_read)
	}
}

func makeNotifiableReceivedMessage(sender, phone, content, received string) string {
	return fmt.Sprintf(
		"SMSを受信しました\n"+
			"送信者:%s\n"+
			"電話番号:%s\n"+
			"受信日時:%s\n"+
			"テキスト:\n%s\n",
		sender, phone, received, content,
	)
}
