module forwardsms

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.7.1 // indirect
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/comail/colog v0.0.0-20160416085026-fba8e7b1f46c // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/line/line-bot-sdk-go v7.8.0+incompatible // indirect
	github.com/sclevine/agouti v3.0.0+incompatible // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
)
