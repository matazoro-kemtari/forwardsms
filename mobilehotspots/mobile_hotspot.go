package mobilehotspots

type ReceiveDeletor interface {
	ReceiveSMS() (int, string, string, string, string)
	DeleteSMS(sender, phone, content, received string)
}
