package mobilehotspots

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/sclevine/agouti"
)

type Zte struct{}

func NewZte() *Zte {
	return &Zte{}
}

/*
参考
https://tanabebe.hatenablog.com/entry/2019/12/24/180000
https://qiita.com/h-hiroki/items/04d8c6636968c07a438e

chromeの使い方
https://qiita.com/Azunyan1111/items/b161b998790b1db2ff7a
*/
func (m *Zte) getWebDriver() *agouti.WebDriver {
	driver := agouti.ChromeDriver(
		agouti.ChromeOptions(
			"args", []string{
				"--headless",
				"--disable-gpu",
			},
		),
	)
	return driver
}

func (m *Zte) navigateInBox(driver *agouti.WebDriver) *agouti.Page {
	page, err := driver.NewPage()
	if err != nil {
		log.Fatalln("driver.NewPage:", err)
	}

	// HOMEページを開く
	url := "http://192.168.0.1/login.asp"
	page.Navigate(url)

	// ログインする
	psw := os.Getenv("ADMIN_PSW")
	page.FindByID("psw").Fill(psw)
	page.FindByID("login_Button").Click()

	time.Sleep(time.Second)

	// contentsフレームにフォーカスする
	if err := page.FindByXPath("/html/frameset/frame[2]").SwitchToFrame(); err != nil {
		log.Fatalln("contentsフレームにフォーカスする:", err)
	}

	if err := page.FindByXPath(`//*[@id="tr_sms"]/td/table/tbody/tr/td`).Click(); err != nil {
		log.Fatalln("SMSクリック:", err)
	}

	time.Sleep(time.Microsecond * 500)

	// work area iフレームにフォーカスする
	if err := page.FindByID("work_area").SwitchToFrame(); err != nil {
		log.Fatalln("work area iフレームにフォーカスする:", err)
	}

	return page
}

// SMS受信ページを取得する
func (m *Zte) loadInBoxPage() (string, error) {
	driver := m.getWebDriver()
	defer driver.Stop()
	driver.Start()

	page := m.navigateInBox(driver)
	return page.HTML()
}

// SMSの内容を取得する
func (m *Zte) ReceiveSMS() (int, string, string, string, string) {
	// SMS受信ページを取得する
	html, err := m.loadInBoxPage()
	if err != nil {
		log.Fatalln("SMS受信ページを取得:", err)
	}

	reader := strings.NewReader(html)
	doc, _ := goquery.NewDocumentFromReader(reader)

	// 受信数を取得
	nv := doc.Find("#nv_total").Text()
	reg, _ := regexp.Compile(`\d+`)
	nvCnt, err := strconv.Atoi(reg.FindString(nv))
	if err != nil {
		log.Fatalln("受信数を変換:", err)
	}

	// 空
	if nvCnt == 0 {
		return nvCnt, "", "", "", ""
	}

	// 送信者を取得
	sender := doc.Find("#name_0").Text()
	// 電話番号を取得
	phone := doc.Find("#phone_0").Text()
	// 本文を取得
	content, exists := doc.Find("#content_view_0").Attr("value")
	if !exists {
		log.Fatalln("本文を取得:", err)
	}
	// 受信日時を取得
	received := doc.Find("#time_0").Text()

	return nvCnt, sender, phone, content, received
}

// SMSを削除する
func (m *Zte) DeleteSMS(sender, phone, content, received string) {
	driver := m.getWebDriver()
	defer driver.Stop()
	driver.Start()

	page := m.navigateInBox(driver)

	// 受信数を取得
	nv, err := page.FindByID("nv_total").Text()
	if err != nil {
		log.Fatalln("受信数を取得", err)
	}
	reg, _ := regexp.Compile(`\d+`)
	nvCnt, err := strconv.Atoi(reg.FindString(nv))
	if err != nil {
		log.Fatalln("受信数を変換:", err)
	}

	// 空
	if nvCnt == 0 {
		return
	}

	for i := 0; i < nvCnt; i++ {
		// 送信者を取得
		sourceSender, err := page.FindByID(fmt.Sprintf("name_%d", i)).Text()
		if err != nil {
			log.Fatalln("送信者を取得:", err)
		}
		// 電話番号を取得
		sourcePhone, err := page.FindByID(fmt.Sprintf("phone_%d", i)).Text()
		if err != nil {
			log.Fatalln("電話番号を取得:", err)
		}
		// 本文を取得
		sourceContent, err := page.FindByID(fmt.Sprintf("content_view_%d", i)).Attribute("value")
		if err != nil {
			log.Fatalln("本文を取得:", err)
		}
		// 受信日時を取得
		sourceReceived, err := page.FindByID(fmt.Sprintf("time_%d", i)).Text()
		if err != nil {
			log.Fatalln("受信日時を取得:", err)
		}

		// メッセージを探す
		if sender == sourceSender && phone == sourcePhone && content == sourceContent && received == sourceReceived {
			// チェックを付ける
			if err = page.FindByID(fmt.Sprintf("check%d", i+1)).Check(); err != nil {
				log.Fatalln("チェックを付ける:", err)
			}

			// 削除ボタンを押す
			page.FindByButton("Delete").Click()

			// 確認alertを受け入れる
			if err := page.ConfirmPopup(); err != nil {
				log.Fatalln("確認alertを受け入れる", err)
			}

			time.Sleep(time.Second * 3)

			return
		}
	}
}
