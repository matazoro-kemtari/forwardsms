package mobilehotspots

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/sclevine/agouti"
)

type Huawei struct{}

func NewHuawei() *Huawei {
	return &Huawei{}
}

/*
参考
https://tanabebe.hatenablog.com/entry/2019/12/24/180000
https://qiita.com/h-hiroki/items/04d8c6636968c07a438e

chromeの使い方
https://qiita.com/Azunyan1111/items/b161b998790b1db2ff7a
*/
func (m *Huawei) getWebDriver() *agouti.WebDriver {
	driver := agouti.ChromeDriver(
		agouti.ChromeOptions(
			"args", []string{
				// "--headless",
				// "--disable-gpu",
			},
		),
	)
	return driver
}

func (m *Huawei) navigateInBox(driver *agouti.WebDriver) *agouti.Page {
	page, err := driver.NewPage()
	if err != nil {
		log.Fatalln("driver.NewPage:", err)
	}

	// HOMEページを開く
	url := "http://192.168.1.1/html/login.html"
	page.Navigate(url)

	// ログインする
	psw := os.Getenv("ADMIN_PSW")
	page.FindByXPath(`//*[@id="password"]`).Fill(psw)
	page.FindByXPath(`//*[@id="login_btn"]`).Click()

	time.Sleep(time.Second)

	if err := page.FindByXPath(`//*[@id="sms"]`).Click(); err != nil {
		log.Fatalln("SMSクリック:", err)
	}

	time.Sleep(time.Microsecond * 500)

	return page
}

// SMS受信ページを取得する
func (m *Huawei) loadInBoxPage() (string, error) {
	driver := m.getWebDriver()
	defer driver.Stop()
	driver.Start()

	page := m.navigateInBox(driver)
	return page.HTML()
}

// SMSの内容を取得する
func (m *Huawei) ReceiveSMS() (int, string, string, string, string) {
	// SMS受信ページを取得する
	html, err := m.loadInBoxPage()
	if err != nil {
		log.Fatalln("SMS受信ページを取得:", err)
	}

	reader := strings.NewReader(html)
	doc, _ := goquery.NewDocumentFromReader(reader)

	// 受信数を取得
	nv := doc.Find("#label_inbox_status").Text()
	reg, _ := regexp.Compile(`\d+$`)
	nvCnt, err := strconv.Atoi(reg.FindString(nv))
	if err != nil {
		log.Fatalln("受信数を変換:", err)
	}

	// 空
	if nvCnt == 0 {
		return nvCnt, "", "", "", ""
	}

	// 送信者を取得
	sender := doc.Find(`#sms_table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td.sms_phone_number`).Text()
	// 電話番号を取得
	phone := doc.Find(`#sms_table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td.sms_phone_number`).Text()
	// 本文を取得
	content := doc.Find(`#sms_table > tbody > tr:nth-child(2) > td:nth-child(3) > pre`).Text()
	// 受信日時を取得
	received := doc.Find(`#sms_table > tbody > tr:nth-child(2) > td:nth-child(4) > label`).Text()

	return nvCnt, sender, phone, content, received
}

// SMSを削除する
func (m *Huawei) DeleteSMS(sender, phone, content, received string) {
	driver := m.getWebDriver()
	defer driver.Stop()
	driver.Start()

	page := m.navigateInBox(driver)

	// 受信数を取得
	nv, err := page.FindByXPath(`//*[@id="label_inbox_status"]`).Text()
	if err != nil {
		log.Fatalln("受信数を取得", err)
	}
	reg, _ := regexp.Compile(`\d+$`)
	nvCnt, err := strconv.Atoi(reg.FindString(nv))
	if err != nil {
		log.Fatalln("受信数を変換:", err)
	}

	// 空
	if nvCnt == 0 {
		return
	}

	for i := 0; i < nvCnt; i++ {
		// 送信者を取得
		sourceSender, err := page.FindByXPath(fmt.Sprintf(`//*[@id="sms_table"]/tbody/tr[%d]/td[2]/table/tbody/tr/td[2]`, i+2)).Text()
		if err != nil {
			log.Fatalln("送信者を取得:", err)
		}
		// 電話番号を取得
		sourcePhone, err := page.FindByXPath(fmt.Sprintf(`//*[@id="sms_table"]/tbody/tr[%d]/td[2]/table/tbody/tr/td[2]`, i+2)).Text()
		if err != nil {
			log.Fatalln("電話番号を取得:", err)
		}
		// 本文を取得
		sourceContent, err := page.FindByXPath(fmt.Sprintf(`//*[@id="sms_table"]/tbody/tr[%d]/td[3]/pre`, i+2)).Text()
		if err != nil {
			log.Fatalln("本文を取得:", err)
		}
		// 受信日時を取得
		sourceReceived, err := page.FindByXPath(fmt.Sprintf(`//*[@id="sms_table"]/tbody/tr[%d]/td[4]/label`, i+2)).Text()
		if err != nil {
			log.Fatalln("受信日時を取得:", err)
		}

		// メッセージを探す
		if sender == sourceSender && phone == sourcePhone && strings.TrimRight(content, "\n") == strings.TrimRight(sourceContent, "\n") && received == sourceReceived {
			// チェックを付ける
			if err = page.FindByXPath(fmt.Sprintf(`//*[@id="sms_table"]/tbody/tr[%d]/td[1]/input`, i+2)).Check(); err != nil {
				log.Fatalln("チェックを付ける:", err)
			}

			// 削除ボタンを押す
			page.FindByXPath(`//*[@id="del_msg_btn"]`).Click()

			// 確認alertを受け入れる
			if err := page.FindByXPath(`//*[@id="pop_confirm"]/span/span/span/a`).Click(); err != nil {
				log.Fatalln("確認alertを受け入れる", err)
			}

			time.Sleep(time.Second * 3)

			return
		}
	}
}
