# ForwardSMS #

SMS付きデータ専用SIMを挿したZTE ポータブルwifiは、SMSを見るためにポータブルwifiの管理画面に入る必要があります。。
受信時も通知されるわけではないので不便です。
これを解消するために、自動でSMSの情報をLINEに転送する機能を提供します。

Raspberry Piでの運用を想定して作成しましたが、Goでプログラムしたため、OS・アーキテクチャの限定なく実行可能です。(テストをしたわけではありませんが)

## 依存関係

* LINEアカウント
* Chrome または Chromium
* WebDriver(ChromeDriver)

## インストール

### LINEアカウント

LINEアカウントが必要です。
ここでは、解説しませんが、LINE BOTなどのキーワードで検索してください。

### Chrome または Chromium

通常インストール済みなことも多いので、プログラムリストから確認してください。
インストールされていなければ、インストールします。

Raspberry piでは、aptコマンドでインストールができます。

```bash
sudo apt update
sudo apt install chromium-browser
```

### WebDriver(ChromeDriver)

[ChromeDriver](https://sites.google.com/chromium.org/driver/)のページで、自分のChromeとバージョンが一致するドライバのバージョンを確認してください。
Raspberry piでは、aptコマンドでインストール可能。ただしChromium(Chrome)のバージョンの確認は必要です。

```Bash
sudo apt update
sudo apt install chromium-chromedriver
```

### ForwardSMS環境変数設定ファイル

新規にファイルを作成し、ご自身の環境に合わせて設定ファイルを用意します。
Buildした実行ファイルと同じフォルダに保存する必要があります。

```
DEVICE_TYPE=XXXXX                        # ポータブルwifiの機種 {ZTE|HUAWEI}
ADMIN_PSW=XXXX                           # ポータブルwifiの管理画面パスワード
LINE_BOT_CHANNEL_SECRET=XXXXXXXX         # LINEアカウント登録時に発行されるチャンネルシークレット
LINE_BOT_CHANNEL_TOKEN=XXXXXXXXX         # LINEアカウント登録時に発行されるアクセストークン
```

## 動作

1回の実行で、ポータブルwifiの受信箱1ページ分のメッセージを、LINEに転送して削除します。
おそらく削除を行うことで、2ページ目以降のメッセージも1ページ目に繰り上がってくると期待されるため、全てのメッセージが転送されると思います。(これはテストしていません)

定期的な実行機能は備えていません。
定期実行は、スケジューラで行ってください。


### 注意点

転送から削除までの間に、削除対象のメッセージが2ページ目以降に繰り下がってしまった場合、削除できないことがあります。
