# License

Please adopt any license that suits your language.

## NYSL Version 0.9982

A. 本ソフトウェアは Everyone'sWare です。このソフトを手にした一人一人が、  
   ご自分の作ったものを扱うのと同じように、自由に利用することが出来ます。

  A-1. フリーウェアです。作者からは使用料等を要求しません。  
  A-2. 有料無料や媒体の如何を問わず、自由に転載・再配布できます。  
  A-3. いかなる種類の 改変・他プログラムでの利用 を行っても構いません。  
  A-4. 変更したものや部分的に使用したものは、あなたのものになります。  
       公開する場合は、あなたの名前の下で行って下さい。

B. このソフトを利用することによって生じた損害等について、作者は  
   責任を負わないものとします。各自の責任においてご利用下さい。  

C. 著作者人格権は Wentz7867 に帰属します。著作権は放棄します。

D. 以上の３項は、ソース・実行バイナリの双方に適用されます。

[NYSL](https://www.kmonos.net/nysl/)

## WTFPL Version 2

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified  
 copies of this license document, and changing it is allowed as long  
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

[WTFPL](http://www.wtfpl.net/)
